FROM python:3.8-alpine as base
FROM base as builder

RUN mkdir /install
WORKDIR /install
COPY requirements.txt /requirements.txt
RUN pip3 install -r /requirements.txt --install-option="--prefix=/install"

FROM base
COPY --from=builder /install /usr/local
COPY search_api /app/search_api

WORKDIR /app

EXPOSE 8000

CMD ["gunicorn", "-b", "0.0.0.0:8000", "search_api.__main__:app", "-w", "5", "--access-logfile", "-", "--access-logformat", "\"%({X-Real-IP}i)s\" (%(h)s) %(l)s %(u)s %(t)s \"%(r)s\" %(s)s %(b)s \"%(f)s\" \"%(a)s\""]