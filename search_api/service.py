#!/usr/bin/python3
import logging
import os
import random
from collections import defaultdict
from typing import List, Tuple, Optional

from flask_pymongo import PyMongo
from pymongo.database import Database


class SearchService:
    AT_BASE = 'https://www.autotrader.co.uk/'
    BASE_URL = os.environ['BASE_URL']
    ITEMS_PER_PAGE = 10

    def __init__(self, mongo: PyMongo):
        self._database: Database = mongo.db
        self.vehicle_collection = self._database.get_collection('vehicles')
        self.ad_collection = self._database.get_collection('ads')
        self._live_search_data = self._get_live_search_data()

    def _get_live_search_data(self) -> dict:
        aggregation_result = self.vehicle_collection.aggregate([
            {
                '$project': {
                    'make': '$api_data.vehicle.make',
                    'model': '$api_data.vehicle.model',
                    'trim': '$api_data.vehicle.trim'
                }
            },
            {
                '$group': {
                    '_id': '$make',
                    'modelTrims': {
                        '$addToSet': {
                            'model': '$model',
                            'trim': '$trim'}
                    }
                }
            }
        ])

        makes = defaultdict(dict)
        for aggregation_document in aggregation_result:
            models = defaultdict(list)

            for model_trim in aggregation_document['modelTrims']:
                model_name = model_trim['model'].lower()
                if trim := model_trim.get('trim'):
                    models[model_name].append(trim.lower())
                else:
                    # Force an empty list to be created
                    models[model_name] = models[model_name]
            makes[aggregation_document['_id'].lower()] = dict(models)
        return dict(makes)

    @staticmethod
    def _create_vehicle_response_object(vehicle: dict, image_size: str) -> dict:
        vehicle_api_data = vehicle['api_data']['vehicle']
        return {
            'title': vehicle['title'],
            'description': vehicle['description'],
            'make': vehicle_api_data['make'],
            'model': vehicle_api_data['model'],
            'price': vehicle['price'],
            'keywords': vehicle['keywords'],
            'url': SearchService.AT_BASE + vehicle['slug'],
            'image_urls': [SearchService.BASE_URL + '/img/vehicles/' + u.replace('{size}', 'full') for u in vehicle['image_urls']],
            'thumb_urls': [SearchService.BASE_URL + '/img/vehicles/' + u.replace('{size}', image_size) for u in vehicle['image_urls']],
            'relevance': vehicle['score']
        }

    def _find_ad(self, query: str) -> Optional[dict]:
        ads = list(self.ad_collection.find({'keywords': {'$in': query.split(' ')}}))

        if len(ads) > 0:
            selected_ad = random.choice(ads)
            selected_ad['image_url'] = SearchService.BASE_URL + '/img/' + selected_ad['image_url']
            selected_ad['url'] = SearchService.AT_BASE + selected_ad['slug']
            selected_ad.pop('slug')
            return selected_ad
        else:
            return None

    def search(self, query: str, page: int = 1) -> Tuple[List[dict], int, Optional[dict]]:
        page_actual = page - 1
        database_results = self.vehicle_collection.find(
            {'$text': {'$search': query}},
            {'score': {'$meta': 'textScore'}}
        ).sort(
            [('score', {'$meta': 'textScore'})]
        ).skip(page_actual * SearchService.ITEMS_PER_PAGE).limit(SearchService.ITEMS_PER_PAGE)

        ad = self._find_ad(query)

        return [SearchService._create_vehicle_response_object(v, 'large') for v in database_results], database_results.count(), ad

    def live_search(self, query: str) -> List[str]:
        query = query.lower()
        parts = query.split()

        if len(parts) == 1:
            term = parts[0]

            if make := self._live_search_data.get(term):
                return list(make.keys())
            else:
                return SearchService._closest_matching_keys(self._live_search_data, term)

        if len(parts) == 2:
            possible_make, possible_model = parts
            if make := self._live_search_data.get(possible_make):
                if model_trims := make.get(possible_model):
                    return model_trims
                else:
                    return SearchService._closest_matching_keys(make, possible_model)
            else:
                return SearchService._closest_matching_keys(self._live_search_data, query)

        if len(parts) == 3:
            possible_make, possible_model, possible_trim = parts
            if make := self._live_search_data.get(possible_make):
                if model_trims := make.get(possible_model):
                    return [m for m in model_trims if m.startswith(possible_trim)]
                else:
                    return SearchService._closest_matching_keys(make, possible_model)
            else:
                return SearchService._closest_matching_keys(self._live_search_data, query)

        return []

    @staticmethod
    def _closest_matching_keys(data: dict, term: str) -> List[str]:
        return [k for k in data.keys() if k.startswith(term)]
