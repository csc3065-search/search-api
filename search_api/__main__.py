#!/usr/bin/python3
import os

from flask import Flask, request, jsonify
from flask_pymongo import PyMongo

from search_api.service import SearchService

app = Flask(__name__)
app.config['MONGO_URI'] = os.environ['MONGODB_URI']
app.config['JSON_SORT_KEYS'] = False

mongo = PyMongo(app)
search_service = SearchService(mongo)


@app.route('/search')
def search():
    user_query = request.args.get('q')
    page = request.args.get('page', 1)

    try:
        page = int(page)
    except ValueError:
        return jsonify(
            success=False,
            error='Query argument \'page\' must be an integer or omitted.',
            data=None
        )

    if not user_query or len(user_query.strip()) == 0:
        return jsonify(
            successs=False,
            error='Missing query argument \'q\'.',
            data=None)

    results, total_items, ad = search_service.search(user_query, page)
    return jsonify(
        success=True,
        error=None,
        data=results,
        ad=ad,
        total=total_items
    )


@app.route('/liveSearch')
def live_search():
    user_query = request.args.get('q')

    if not user_query or len(user_query.strip()) == 0:
        return jsonify(
            successs=False,
            error='Missing query argument \'q\'.',
            data=None)

    return jsonify(
        success=True,
        error=None,
        data=search_service.live_search(user_query))


@app.route('/healthz')
def health():
    return jsonify({'status': 'UP'})


if __name__ == '__main__':
    app.run()
